require_relative "./database_with_cache"
require "rspec/mocks"

describe DatabaseWithCache do
	before(:each) do
	
		@books = [Book.new('1111','title 1','author 1',12.99, 'Programming', 20 ), Book.new('1112','title 2','author 1',200.99, 'Programming', 11 )]
		@memcached_mock = double()
		@database_mock = double()
		@target = DatabaseWithCache.new @database_mock, @memcached_mock 
		@local_cache = @target.instance_variable_get "@local_cache"
		
	end

	describe "#isbnSearch" do
		context "Given the book ISBN is valid" do
			context "and it is not in the local cache" do
				context "nor in the remote cache" do
					it "should read it from the database and add it to the remote cache" do
					
						result = isbnSearchInTest
						expect(result).to be @books[0]
						
					end
				end

				context "but it is in the remote cache" do
					it "should use the remote cache version and add it to local cache" do
					
						expect(@database_mock).to_not receive(:isbnSearch)
						expect(@memcached_mock).to receive(:get).with("v_#{@books[0].isbn}").and_return 1
						expect(@memcached_mock).to receive(:get).with("#{@books[0].isbn}_1").
						and_return @books[0].to_cache 
						expect(@target.isbnSearch(@books[0].isbn)).to eq @books[0]

						# Check The If The Book Was Added To The Local Cache
						expect((@local_cache.get @books[0].isbn)[:book]).to eq @books[0]
						expect((@local_cache.get @books[0].isbn)[:version]).to eq 1
						
					end
				end 	
			end        

			context "and it is in the local cache" do
				context "and up to date with the remote cache" do
					it "should use the local cache version" do

						expect(@database_mock).to_not receive(:isbnSearch)
						
						# Setting Up Local Cache Initially
						localCacheTestInitSet
						
						# Attempting The Search Again
						expect(@local_cache).to receive(:get).ordered.with(@books[0].isbn).and_return({book: @books[0], version: 1})
						expect(@memcached_mock).to receive(:get).ordered.with("v_#{@books[0].isbn}").and_return 1
						@target.isbnSearch(@books[0].isbn)

					end
				end
				
				context "and not up to date with the remote cache" do
					it "should use the remote cache version and update the local cache" do
						
						expect(@database_mock).to_not receive(:isbnSearch)
						
						# Setting Up Local Cache Initially
						localCacheTestInitSet
						
						# Setting Update Data
                        upd_book1111 = preDefUpdData
                        
						# Updating Book
						updBookInTest upd_book1111
						
						# Attempting To Retrieve The Book Data
						expect(@local_cache).to receive(:get).ordered.with(@books[0].isbn).and_return({book: @books[0], version: 1})
						expect(@memcached_mock).to receive(:get).ordered.with("v_#{@books[0].isbn}").and_return 2
						expect(@memcached_mock).to receive(:get).ordered.with("#{@books[0].isbn}_2").and_return upd_book1111.to_cache
						expect(@local_cache).to receive(:set).ordered.with(@books[0].isbn, { :book => upd_book1111, :version => 2})
						@target.isbnSearch(@books[0].isbn)
						
						# Checking Local Cache Version And Data Validity
						allow(@local_cache).to receive(:get).and_return({ :book => upd_book1111, :version => 2})
						updBookLocalCacheCheck upd_book1111, 2
						
					end
				end
			end
		end
	end

	describe "#updateBook" do
		context "Given the book ISBN is valid," do

			# Update Object
			upd_book1111 = nil

			it "the book should be updated in the local cache" do 

				# Initial Book Object Retrieval
				isbnSearchInTest
		
				# Setting Update Data
				upd_book1111 = preDefUpdData
		
		        # Updating Book
				updBookInTest upd_book1111
				
				# Check If Book Was Updated In Local Cache
				updBookLocalCacheCheck upd_book1111, 2

			end

			it "the updated value should be stored in the database" do

				expect(@database_mock).to receive(:isbnSearch).and_return upd_book1111
				expect(@memcached_mock).to receive(:set)
				expect(@memcached_mock).to receive(:set)
				expect(@memcached_mock).to receive(:get)
				result = @target.isbnSearch(@books[0].isbn)

				# Checking If The Newly Retrieved Version Was Updated
				expect(result).to eq(upd_book1111)
				expect(result.to_s).not_to eq(@books[0].to_s)
				
			end
		end
	end

	describe "#authorSearch" do
		context "Given the author name is valid and" do 
			context "the data is not present in the remote cache and the author has one book," do
				it "the book of the given author should be retrieved from the database", :storVal => 
				"{\"books\":[{\"title\":\"title 1\",\"isbn\":\"1111\"}],\"value\":259.8}" do |example|

					expect(@memcached_mock).to receive(:get).with("bks_#{@books[0].author}")
					expect(@database_mock).to receive(:authorSearch).with(@books[0].author).and_return [@books[0]] # Array Of Belonging Books
					expect(@memcached_mock).to receive(:set).with("bks_#{@books[0].author}", @books[0].isbn)
					
					setRemCacheAuthorSearchTest 0
					
					expect(@memcached_mock).to receive(:set).with("#{@books[0].author}_#{@books[0].isbn}_1 ", example.metadata[:storVal]) 

					result = @target.authorSearch(@books[0].author)
					authorSearchReturnValueCheck result
					expect(result["books"]).to eq([{"title"=>"title 1", "isbn"=>"1111"}])

				end
			end

			context "the data is present in the remote cache and the author has one book," do
				it "the book of the given author should be retrieved from the remote cache", :storVal => 
				"{\"books\":[{\"title\":\"title 1\",\"isbn\":\"1111\"}],\"value\":259.8}" do |example|

					expect(@memcached_mock).to receive(:get).with("bks_#{@books[0].author}").and_return @books[0].isbn 
					expect(@memcached_mock).to receive(:get).with("v_#{@books[0].isbn}").and_return 1
					expect(@memcached_mock).to receive(:get).with("#{@books[0].author}_#{@books[0].isbn}_1 ").and_return example.metadata[:storVal]

					authorSearchReturnValueCheck @target.authorSearch(@books[0].author)

				end
			end

			context "the data is not present in the remote cache and the number of books is more than one," do
				it "the books of the given author should be retrieved from the database and the remote cache data should be set", :multiVal =>
				"{\"books\":[{\"title\":\"title 1\",\"isbn\":\"1111\"},{\"title\":\"title 2\",\"isbn\":\"1112\"}],\"value\":2470.6900000000005}" do |example|

					expect(@memcached_mock).to receive(:get).with("bks_#{@books[0].author}")
					
					setRemCacheAuthorSearchTest 0
					setRemCacheAuthorSearchTest 1

					expect(@memcached_mock).to receive(:set).with("#{@books[0].author}_#{@books[0].isbn}_1_#{@books[1].isbn}_1 ", 
					example.metadata[:multiVal])
					expect(@database_mock).to receive(:authorSearch).with(@books[0].author).and_return [@books[0], @books[1]] # Array Of Belonging Books
					expect(@memcached_mock).to receive(:set).with("bks_#{@books[0].author}", "#{@books[0].isbn},#{@books[1].isbn}")

					authorSearchReturnValueCheck @target.authorSearch(@books[0].author)

				end
			end

			context "the data is present in the remote cache and the number of books is more than one," do
				it "the books of the given author should be retrieved from the remote cache", :multiVal =>
				"{\"books\":[{\"title\":\"title 1\",\"isbn\":\"1111\"},{\"title\":\"title 2\",\"isbn\":\"1112\"}],\"value\":2470.6900000000005}" do |example|
				
					expect(@memcached_mock).to receive(:get).with("bks_#{@books[0].author}").and_return "#{@books[0].isbn},#{@books[1].isbn}" 
					expect(@memcached_mock).to receive(:get).with("v_#{@books[0].isbn}").and_return 1
					expect(@memcached_mock).to receive(:get).with("v_#{@books[1].isbn}").and_return 1
					expect(@memcached_mock).to receive(:get).with("#{@books[0].author}_#{@books[0].isbn}_1_#{@books[1].isbn}_1 ").
					and_return example.metadata[:multiVal]

					authorSearchReturnValueCheck @target.authorSearch(@books[0].author)
					
				end
			end        	

			context "books prices and quantities were updated," do
				it "the updated values should be retrieved from the database", :updVal =>
				"{\"books\":[{\"title\":\"title 1\",\"isbn\":\"1111\"}],\"value\":5000}" do |example|

					# Setting Update Data
					@books[0].price = 50
					@books[0].quantity = 100

					# Updating Book
					updBookInTest @books[0]

					expect(@memcached_mock).to receive(:get).with("bks_#{@books[0].author}").and_return @books[0].isbn 
					expect(@memcached_mock).to receive(:get).with("v_#{@books[0].isbn}").and_return 2
					expect(@memcached_mock).to receive(:get).with("#{@books[0].author}_#{@books[0].isbn}_2 ").and_return example.metadata[:updVal]

					result = @target.authorSearch(@books[0].author)
					authorSearchReturnValueCheck result
					expect(result).to eq({"books"=>[{"title"=>"title 1", "isbn"=>"1111"}], "value"=>5000})

				end
			end
		end
	end

	private

	def isbnSearchInTest  

		expect(@memcached_mock).to receive(:get).ordered.with('v_' + @books[0].isbn).and_return nil
		expect(@memcached_mock).to receive(:set).ordered.with('v_' + @books[0].isbn,1)
		expect(@memcached_mock).to receive(:set).ordered.with(@books[0].isbn + '_1',@books[0].to_cache)
		expect(@database_mock).to receive(:isbnSearch).with(@books[0].isbn).
		and_return(@books[0])
		result = @target.isbnSearch(@books[0].isbn)
		result
		
	end
	
	def localCacheTestInitSet
	
		expect(@memcached_mock).to receive(:get).with("v_#{@books[0].isbn}").and_return 1
		expect(@memcached_mock).to receive(:get).with("#{@books[0].isbn}_1").
		and_return @books[0].to_cache 
		@target.isbnSearch(@books[0].isbn)
		expect(@local_cache).to_not eq(nil)
		
	end
	
	def preDefUpdData
	
		# Creating Updated Book Object
		result = @books[0].dup
		result.price = 24.99
		result.author = "Joe Bloggs"
		result.title = "C++ In 3 Hours"
		result
		
	end
	
	def updBookInTest updBook
		
		# Updating Book
		expect(@database_mock).to receive(:updateBook)
		# Remote Cache Is Updated As Well
		expect(@memcached_mock).to receive(:get).with("v_#{updBook.isbn}").and_return 1
		expect(@memcached_mock).to receive(:set).with("v_#{updBook.isbn}", 2)
		expect(@memcached_mock).to receive(:set).with("#{updBook.isbn}_2", updBook.to_cache)
		@target.updateBook updBook
				
	end
	
	def updBookLocalCacheCheck book, version
	
		expect((@local_cache.get @books[0].isbn)[:book]).to eq book
		expect((@local_cache.get @books[0].isbn)[:version]).to eq version
						
	end
	
	def setRemCacheAuthorSearchTest bookNum
	
		expect(@memcached_mock).to receive(:get).with("v_#{@books[bookNum].isbn}")
		expect(@memcached_mock).to receive(:set).with("v_#{@books[bookNum].isbn}", 1)
		expect(@memcached_mock).to receive(:set).with("#{@books[bookNum].isbn}_1", @books[bookNum].to_cache)
		
	end

	def authorSearchReturnValueCheck result

		totalVal = 0

		result["books"].each{|v|
		@books.each{|b|
		if b.isbn == v["isbn"] 
		totalVal += b.quantity * b.price
		end}}

		expect(result["value"]).to eq(totalVal)
		
	end

end


